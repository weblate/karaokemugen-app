# Résumé

(décrivez de façon concise le bug rencontré)

# Quelle version de Karaoke Mugen ?

(indiquez la version ici)

# Quel OS ?

(Windows/Linux/Mac)

(Si Linux, quelle distribution?)

# Comment le reproduire

(si possible, indiquez comment reproduire le bug)

# Qu'est-ce qu'il se passe actuellement comme bug ?

(Racontez-nous tout)

# Qu'est-ce qu'il devrait plutôt se passer si tout marchait bien ?

(Ce à quoi vous vous attendiez)

# Logs et/ou captures d'écrans

(Joignez votre karaokemugen.log et/ou une capture d'écran du bug rencontré)
